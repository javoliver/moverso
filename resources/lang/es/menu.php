<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/30/2017
 * Time: 10:12 AM
 */

return [
    'home' => 'INICIO',
    'recent_work' => 'TRABAJO RECIENTE',
    'portfolio' => 'PORTAFOLIO',
    'services' => 'SERVICIOS',
    'team' => 'EQUIPO',
    'contacts' => 'CONTACTOS',
];