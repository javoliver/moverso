<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben ser de al menos seis caracteres y coincidir con la confirmación.',
    'reset' => 'Su contraseña ha sido modificada!',
    'sent' => 'Le enviamos un enlace a su correo con su contraseña modificada!',
    'token' => 'Este token de restablecimiento de contraseña no es válido.',
    'user' => "No podemos encontrar un usuario con esta dirección de correo electrónico.",

];
