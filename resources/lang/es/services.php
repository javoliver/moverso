<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:23 PM
 */


return [
    'direction' => 'DIRECCIÓN',
    'photographhy' => 'FOTOGRAFÍA',
    'creativity' => 'SERVICIOS DE CREATIVIDAD',
    'graphic' => 'DISEÑO GRÁFICO',
    'castings' => 'CASTINGS',
    'locations' => 'UBICACIONES',
    'equipments' => 'EQUIPOS',
    'permissions' => 'PERMISOS',
    'peak' => '',
];