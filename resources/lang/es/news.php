<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:32 PM
 */


return [
    'minutes_ago' => 'hace :minutes minutos',
    'seconds_ago' => 'hace :seconds segundos',
    'days_ago' => 'hace :days días',
    'months_ago' => 'hace :months meses',
    'years_ago' => 'hace :years años',
    'days_left' => ':days restantes',
    'months_left' => ':months restantes',
    'see_more' => 'SEE MORE',
];