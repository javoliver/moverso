<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:28 PM
 */


return [
    'direction' => 'DIRECCIÓN',
    'photography' => 'FOTOGRAFÍA',
    'production' => 'PRODUCCIÓN',
    'postproduction' => 'POSTPRODUCCIÓN',
    'play' => 'PLAY VIDEO',
    'making' => 'MAKING OF',
    'photos' => 'FOTOS',
    'all' => 'TODOS',
    'see_more' => 'VER MÁS',
];