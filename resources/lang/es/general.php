<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:13 PM
 */

return [
    'share' => 'COMPARTIR',
    'covering_title' => 'UNIVERSO EN MOVIMIENTO',
    'see_work_button' => 'VER NUESTRO TRABAJO',
    'more_about_us_button' => 'MÁS ACERCA DE NOSOTROS',
    'error_message' => 'Hubo algunos problemas con los datos introducidos.',
    'contact_form' => 'Formulario de contacto',
    'attention' => 'Attention!!!',
    'message_send' => 'Su mensaje ha sido enviado, pronto responderemos a su solicitud.',
];