<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 12:45 PM
 */

return [
    'video' => 'VIDEOS',
    'production' => 'PRODUCTIONS',
    'photography' => 'PHOTOGRAPHY',
    'graphic_design' => 'GRAPHIC DESIGN',
    'what' => 'WHAT',
    'we_do' => 'WE DO',
];
