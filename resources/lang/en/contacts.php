<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:38 PM
 */


return [
    'name' => 'NAME',
    'email' => 'EMAIL',
    'phone' => 'PHONE',
    'subject' => 'SUBJECT',
    'message' => 'MESSAGE',
    'send_mail' => 'SEND EMAIL',
];