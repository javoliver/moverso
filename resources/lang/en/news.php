<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:32 PM
 */

return [
    'minutes_ago' => ':minutes minutes ago',
    'seconds_ago' => ':seconds seconds ago',
    'days_ago' => ':days days ago',
    'months_ago' => ':months months ago',
    'years_ago' => ':years years ago',
    'days_left' => ':days days left',
    'months_left' => ':months months left',
    'see_more' => 'SEE MORE',
];