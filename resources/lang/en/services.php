<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:23 PM
 */

return [
    'direction' => 'DIRECTION',
    'photographhy' => 'PHOTOGRAPHY',
    'creativity' => 'CREATIVITY SERVICES',
    'graphic' => 'GRAPHIC DESIGN',
    'castings' => 'CASTINGS',
    'locations' => 'LOCATIONS',
    'equipments' => 'EQUIPMENTS',
    'permissions' => 'PERMISSIONS',
    'peak' => 'TAKE A PEAK',
];