<?php
/**
 * Created by PhpStorm.
 * User: Yania
 * Date: 6/28/2017
 * Time: 2:28 PM
 */

return [
    'direction' => 'DIRECTION',
    'photography' => 'PHOTOGRAPHY',
    'production' => 'PRODUCTION',
    'postproduction' => 'POSTPRODUCTION',
    'play' => 'PLAY VIDEO',
    'making' => 'MAKING OF',
    'photos' => 'PHOTOS',
    'all' => 'ALL',
    'see_more' => 'SEE MORE',
];