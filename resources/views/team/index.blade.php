@extends('layouts.app')

@section('content')
    <div class="container news-page">
        <div class="row">
            <div class="col-md-12">
                <div class="table-header">
                    <h4>Managing team members</h4>
                    <a href="{{url('/team/create')}}">
                        <button type="button" class="btn btn-sm btn-default new-button">Add new</button>
                    </a>
                    <p id="message-member"></p>
                </div>
                <div class="table-responsive">
                    <div class="panel-body">
                        <table id="table_member" class="table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Last Name</th>
                                <th>Role</th>
                                <th>Responsability</th>
                                <th>Web address</th>
                                <th>Photo</th>
                                <th>Actions</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $counter = 0; if($members == '[]')$member = new App\Member();?>
                            @foreach($members as $member)
                                <tr id="{{$member->id}}">
                                    <td><?php echo ++$counter;?></td>
                                    <td>{{$member->name}}</td>
                                    <td>{{$member->last_name}}</td>
                                    <td>{{$member->role->spanish_role}}</td>
                                    <td>{{$member->profession->profession_en}}</td>
                                    <td>{{$member->web_address}}</td>
                                    <td><a href="#see-member-image" role="button" data-toggle="modal" data-imageurl="{{$member->photo}}" class="btn btn-xs btn-default">See image</a></td>
                                    <td class="actions">
                                        <a href="/team/{{$member->id}}/edit" class="btn btn-xs btn-success">Edit</a>
                                    </td>
                                    <td class="actions">
                                        <button type="button" data-target="#confirmation-message-delete-member" data-toggle="modal" class="btn btn-xs btn-danger" data-ident="{{$member->id}}">Delete</button>
                                        {{--<a href="#confirmation-message-delete" role="button" data-toggle="modal" data-ident="{{$news->id}}" class="btn btn-xs btn-danger">Delete</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: confirmation message-->
    <div class="modal fade" id="confirmation-message-delete-member" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMemberD">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelMemberD">Confirmation message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this member?</p>
                </div>
                <div class="modal-footer">
                    {{csrf_field()}}
                    {{method_field('delete')}}
                    <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm button-cancel" id="delete-member-confirm">Delete</button>
                </div>
            </div>
        </div>
    </div>

    <
    <!-- Modal: see image-->
    <div class="modal fade" id="see-member-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabelMemberI">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img src="" alt="" class="modal-image">
            </div>
        </div>
    </div>



@endsection
