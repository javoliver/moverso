@extends('layouts.app')


@section('content')
    <div class="row-fluid ">
        <div class="col-sm-10 col-sm-offset-1 create-news-form">
            <div class="page-header">
                <h4>Editing the information of the selected member</h4>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong> @lang('general.error_message')</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form id="member-form" class="form-inline" action="/team/{{$member->id}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="control-group separate-group">
                    <div class="col-sm-2">
                        <label for="member-name" class="control-label">Name</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('member_name') ? 'has-error' : '' }}">
                        <input name="member_name" id="member-name" class="span12" type="text" value="{{$member->name}}" required>
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="member-last-name" class="control-label">Last Name</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('member_last_name') ? 'has-error' : '' }}">
                        <input name="member_last_name" id="member-last-name" class="span12" type="text" value="{{$member->last_name}}" required>
                    </div>
                </div>


                <div class="control-group separate-group">
                    <div class="col-sm-2">
                        <label for="member-role" class="control-label">Role</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('member_role') ? 'has-error' : '' }}">
                        <select name="member_role" id="member-role" class="span12">
                            <option value=""></option>
                            @foreach($roles as $role)
                                <option value="{{$role->id}}" <?php if($member->role_id == $role->id) echo 'selected';?>>{{$role->english_role}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="member-profession" class="control-label">Profession</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('member_profession') ? 'has-error' : '' }}">
                        <select name="member_profession" id="member-profession" class="span12">
                            <option value=""></option>
                            @foreach($professions as $profession)
                                <option value="{{$profession->id}}" <?php if($member->profession_id == $profession->id) echo 'selected';?>>{{$profession->profession_en}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="control-group separate-group">
                    <div class="col-sm-2">
                        <label for="member-web-address" class="control-label">Web Address</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('member_web_address') ? 'has-error' : '' }}">
                        <input name="member_web_address" id="member-web-address" class="span12" type="text" value="{{$member->web_address}}" required>
                    </div>

                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="member-photo" class="control-label">Photo</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('member_photo') ? 'has-error' : '' }}">
                        <div class="upload">
                            <a href="#" class="btn btn-default span12" role="button">
                                <span class="glyphicon glyphicon-upload"></span>Select an image.
                            </a>
                        </div>
                        <input name="member_photo" id="member-photo" class="span12" type="file" value="{{$member->photo}}" size="2048">
                    </div>
                </div>

                <div class="control-group col-sm-12 my-buttons">
                    <a href="{{url('/team')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="submit" class="btn btn-success">Edit</button>
                </div>
            </form>
        </div>
    </div>
@endsection