@extends('layouts.app')


@section('content')
    <div class="row-fluid">
        <div class="col-sm-10 col-sm-offset-1 create-news-form">
            <div class="page-header">
                <h4>Creating a profession</h4>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong> @lang('general.error_message')</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form class="form-inline" action="{{url('/profession')}}" method="post">
                {{ csrf_field() }}
                {{method_field('POST')}}

                <div class="control-group separate-group ">
                    <div class="col-sm-2">
                        <label for="spanish-profession" class="control-label">Spanish Profession</label>
                    </div>
                    <div class="col-sm-2 {{ $errors->has('spanish_profession') ? 'has-error' : '' }}">
                        <input name="spanish_profession" id="spanish-profession" class="span12" type="text" value="{{old('spanish_profession')}}" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="english-profession" class="control-label">English Profession</label>
                    </div>
                    <div class="col-sm-2 {{ $errors->has('english_profession') ? 'has-error' : '' }}">
                        <input name="english_profession" id="english-profession" class="span12" type="text" value="{{old('english_profession')}}" oninvalid="InvalidMsg(this);" oninput="InvalidMsg(this);" required>
                    </div>
                </div>

                <div class="control-group col-sm-12 my-buttons">
                    <a href="{{url('/profession')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
        </div>
    </div>
@endsection
