@extends('layouts.app')

@section('content')
    <div class="container news-page">
        <div class="row">
            <div class="col-md-12">
                <div class="table-header">
                    <h4>Managing roles</h4>
                    <a href="{{url('/roles/create')}}">
                        <button type="button" class="btn btn-sm btn-default new-button">Add new</button>
                    </a>
                </div>
                <p id="message-role" class=""></p>
                <div class="table-responsive">
                    <div class="panel-body">
                        <table id="table_role" class="table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Spanish Role</th>
                                <th>English Role</th>
                                <th>Actions</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $counter = 0; if($roles == '[]')$role = new App\role();?>
                            @foreach($roles as $role)
                                <tr id="{{$role->id}}">
                                    <td><?php echo ++$counter;?></td>
                                    <td>{{$role->spanish_role}}</td>
                                    <td>{{$role->english_role}}</td>
                                    <td class="actions">
                                        <a href="/roles/{{$role->id}}/edit" class="btn btn-xs btn-success">Edit</a>
                                    </td>
                                    <td class="actions">
                                        <button type="button" data-target="#confirmation-message-delete-role" data-toggle="modal" class="btn btn-xs btn-danger" data-ident="{{$role->id}}">Delete</button>
                                        {{--<a href="#confirmation-message-delete" role="button" data-toggle="modal" data-ident="{{$news->id}}" class="btn btn-xs btn-danger">Delete</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: confirmation message-->
    <div class="modal fade" id="confirmation-message-delete-role" tabindex="-1" role="dialog" aria-labelledby="myModalLabelroleD">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelroleD">Confirmation message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this element?</p>
                </div>
                <div class="modal-footer">
                    {{csrf_field()}}
                    {{method_field('delete')}}
                    <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm button-cancel" id="delete-role-confirm">Delete</button>
                </div>
            </div>
        </div>
    </div>

@endsection
