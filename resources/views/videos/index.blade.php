@extends('layouts.app')

@section('content')
    <div class="container news-page">
        <div class="row">
            <div class="col-md-12">
                <div class="table-header">
                    <h4>Managing videos</h4>
                    <a href="{{url('/videos/create')}}">
                        <button type="button" class="btn btn-sm btn-default new-button">Add new</button>
                    </a>
                    <p id="message1"></p>
                </div>
                <div class="table-responsive">
                    <div class="panel-body">
                        <table id="table_videos" class="table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Artists</th>
                                <th>Making off URL</th>
                                <th>Video URL</th>
                                <th>Category</th>
                                <th>Priority</th>
                                <th>Photos</th>
                                <th>Actions</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $counter = 0; if($videos == '[]')$video = new App\Video();?>
                            @foreach($videos as $video)
                                <tr id="{{$video->id}}">
                                    <td><?php echo ++$counter;?></td>
                                    <td>{{$video->title}}</td>
                                    <td>{{$video->artists}}</td>
                                    <td>{{$video->making_off_url}}</td>
                                    <td>{{$video->video_url}}</td>
                                    <td>{{$video->category}}</td>
                                    <td>{{$video->priority}}</td>
                                    <td><a href="/videosphotoss/{{$video->id}}" class="btn btn-xs btn-default">Manage photos</a></td>
                                    <td class="actions">
                                        <a href="/videos/{{$video->id}}/edit" class="btn btn-xs btn-success">Edit</a>
                                    </td>
                                    <td class="actions">
                                        <button type="button" data-target="#video-confirmation-message-delete" data-toggle="modal" class="btn btn-xs btn-danger" data-identif="{{$video->id}}">Delete</button>
                                        {{--<a href="#confirmation-message-delete" role="button" data-toggle="modal" data-ident="{{$news->id}}" class="btn btn-xs btn-danger">Delete</a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: confirmation message-->
    <div class="modal fade" id="video-confirmation-message-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabelVideo">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelVideo">Confirmation message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this video information? If you delete this video, all the photos related with it will be also deleted.</p>
                </div>
                <div class="modal-footer">
                    {{csrf_field()}}
                    {{method_field('delete')}}
                    <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm button-cancel" id="video-delete-confirm">Delete</button>
                </div>
            </div>
        </div>
    </div>


@endsection
