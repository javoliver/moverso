@extends('layouts.app')


@section('content')
    <div class="row-fluid ">
        <div class="col-sm-10 col-sm-offset-1 create-news-form">
            <div class="page-header">
                <h4>Editing the information of the selected element</h4>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong> @lang('general.error_message')</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-inline" action="/contactus/{{$contact->id}}" method="post">
                {{ csrf_field() }}
                {{method_field('PUT')}}
                <div class="control-group separate-group">
                    <div class="col-sm-2">
                        <label for="contactus-subject" class="control-label">Spanish Subject</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('contactus_subject') ? 'has-error' : '' }}">
                        <input name="contactus_subject" id="contactus-subject" class="span12" type="text" value="{{$contact->translate('es')->subject}}" required>
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="contactus-subject-en" class="control-label">English Subject</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('contactus_subject_en') ? 'has-error' : '' }}">
                        <input name="contactus_subject_en" id="contactus-subject_en" class="span12" type="text" value="{{$contact->translate('en')->subject}}" required>
                    </div>
                </div>

                <div class="control-group separate-group">
                    <div class="col-sm-2">
                        <label for="contactus-email" class="control-label">E-mail</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('contactus_email') ? 'has-error' : '' }}">
                        <input name="contactus_email" id="contactus-email" class="span12" type="email" value="{{$contact->email}}" required>
                    </div>
                </div>

                <div class="control-group col-sm-12 my-buttons">
                    <a href="{{url('/contactus')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="submit" class="btn btn-success">Edit</button>
                </div>
            </form>
        </div>
    </div>
@endsection