@extends('layouts.app')

@section('content')
    <div class="container initial-info">
        <div class="row">
            <div class="col-md-12">
                <div class="table-header">
                    <h4>Managing general information</h4>
                </div>
                <p class="alert alert-notification">What do you want to display in the initial view?</p>
                <form class="form-inline" action="{{url('/configuration')}}" method="post">
                    {{ csrf_field() }}
                    {{method_field('POST')}}
                    <div class="col-sm-12 control-group no-padding">
                        <label class="radio">
                            <input type="radio" name="optionsRadios" id="default" value="default" <?php if($configuration->configuration == 'default') echo 'checked';?> >
                            Default view
                        </label>

                    </div>

                    <div class="col-sm-12 control-group no-padding">
                        <label class="radio">
                            <input type="radio" name="optionsRadios" id="video" value="video" <?php if($configuration->configuration == 'video') echo 'checked';?>>
                            Video Background
                        </label>
                    </div>

                    <div class="col-sm-12 control-group no-padding">
                        <div class="col-sm-3 video-label">
                            <label for="video-link" class=""> Video background link</label>
                        </div>
                        <div class="col-sm-8 video-link-div">
                            <input name="video_link" id="video-link" class="full-input" type="text" value="<?php if($configuration->configuration == 'video') {echo $configuration->video_url;} else{echo '';}?>">
                        </div>
                    </div>

                    <div class="col-sm-12 control-group separate-group seew-group no-padding">
                        <div class="col-sm-3">
                            <label for="seew-video-link" class="">See our work video link</label>
                        </div>
                        <div class="col-sm-8">
                            <input name="seew_video_link" id="seew-video-link" class="full-input" type="text" value="{{$configuration->seew_video_link}}" required>
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="col-sm-12 no-padding">
                            <input type="checkbox" name="welcome_image" id="welcome-image" value="true" <?php if($configuration->welcome_image == true) echo 'checked';?>>
                            <label for="welcome-image" class="">Display a welcome image in the "What we do" section?</label>
                        </div>
                    </div>

                    <div class="col-sm-12 control-group separate-group seew-group no-padding">
                        <div class="col-sm-3">
                            <label for="peak-video-link" class="">Take a peak video link</label>
                        </div>
                        <div class="col-sm-8">
                            <input name="peak_video_link" id="peak-video-link" class="full-input" type="text" value="{{$configuration->peak_video_link}}" required>
                        </div>
                    </div>

                    <div class="control-group col-sm-12 my-buttons no-padding">
                        <button type="submit" class="btn btn-success">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
