@extends('layouts.app')


@section('content')
    <div class="row-fluid">
        <div class="col-sm-10 col-sm-offset-1 create-news-form">
            <div class="page-header">
                <h4>Creating a news</h4>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong> @lang('general.error_message')</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form id="news-form" class="form-inline" action="{{url('/news')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{method_field('POST')}}
                <div class="form-group col-sm-12 separate-group">
                    <div class="col-sm-2">
                        <label for="news-title" class="control-label">Spanish Title</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('news_title') ? 'has-error' : '' }}">
                        <input name="news_title" id="news-title" class="span12" type="text" value="{{old('news_title')}}" required>
                    </div>

                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="news-title-en" class="control-label">English Title</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('news_title_en') ? 'has-error' : '' }}">
                        <input name="news_title_en" id="news-title-en" class="span12" type="text" value="{{old('news_title_en')}}" required>
                    </div>
                </div>


                <div class="form-group col-sm-12 separate-group">
                    <div class="col-sm-2">
                        <label for="news-singers" class="control-label">Interprets</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('news_singers') ? 'has-error' : '' }}">
                        <input name="news_singers" id="news-singers" class="span12" type="text" value="{{old('news_singers')}}" required>
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="news-photo" class="control-label">Photo</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('news_photo') ? 'has-error' : '' }}" style="height: 20px;">
                        <div class="upload">
                            <a href="#" class="btn btn-default span12" role="button">
                                <span class="glyphicon glyphicon-upload"></span>Select an image.
                            </a>
                        </div>
                        <input name="news_photo" id="news-photo" class="span12" type="file" value="{{old('news_photo')}}" size="2048" required>
                    </div>
                </div>


                <div class="form-group col-sm-12 separate-group">
                    <div class="col-sm-2">
                        <label for="see-more-link" class="control-label">"See more" link</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('see_more_link') ? 'has-error' : '' }}">
                        <input name="see_more_link" id="see-more-link" class="span12" type="url" value="{{old('see_more_link')}}">
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="news-tag-owner" class="control-label">Owner tag name</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('news_tag_owner') ? 'has-error' : '' }}">
                        <input name="news_tag_owner" id="news-tag-owner" class="span12" type="text" value="{{old('news_tag_owner')}}">
                    </div>
                </div>

                <div class="form-group col-sm-12 separate-group">
                    <div class="col-sm-2">
                        <label for="owner-tag-link" class="control-label">Owner tag link</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('owner_tag_link') ? 'has-error' : '' }}">
                        <input name="owner_tag_link" id="owner-tag-link" class="span12" type="url" value="{{old('owner_tag_link')}}">
                    </div>
                    <div class="col-sm-2 col-sm-offset-2">
                        <label for="news-premiere_date" class="control-label">Premiere date</label>
                    </div>
                    <div class="col-sm-3 {{ $errors->has('news_premiere_date') ? 'has-error' : '' }}">
                        <input name="news_premiere_date" class="span12" id="news-premiere_date"  value="{{old('news_premiere_date')}}" data-date-format="yyyy/mm/dd"  type="text" required>
                    </div>
                </div>

                <div class="form-group col-sm-12 separate-group">
                    <div class="control-group">
                        <label for="news-description" class="control-label">Spanish Description</label>
                    </div>
                    <div class="control-group separate-group {{ $errors->has('news_description') ? 'has-error' : '' }}">
                        <textarea id="news-description" name="news_description" rows="3" class="span12" required>{{old('news_description')}}</textarea>
                        <p class="charactersSpanish"></p>
                    </div>
                </div>

                <div class="form-group col-sm-12 separate-group">
                    <div class="control-group">
                        <label for="news-description-en" class="control-label">English Description</label>
                    </div>
                    <div class="control-group separate-group {{ $errors->has('news_description_en') ? 'has-error' : '' }}">
                        <textarea id="news-description-en" name="news_description_en" rows="3" class="span12" required>{{old('news_description_en')}}</textarea>
                        <p class="charactersEnglish"></p>
                    </div>
                </div>


                <div class="control-group col-sm-12 my-buttons">
                    <a href="{{url('/news')}}">
                        <button type="button" class="btn btn-default">Cancel</button>
                    </a>
                    <button type="submit" class="btn btn-success">Create</button>
                </div>
            </form>
        </div>
    </div>


@endsection
