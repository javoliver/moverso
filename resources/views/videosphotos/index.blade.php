@extends('layouts.app')

@section('content')
    <div class="container news-page">
        <div class="row-fluid">
            <div class="table-header">
                <?php
                    $video = App\Video::find($video_id);
                ?>
                <h4>Managing video photos "{{$video->title}}"</h4>
                <a href="{{url('/videos')}}">
                    <button type="button" class="btn btn-sm btn-default new-button">Return to videos</button>
                </a>
                <p id="message1"></p>
            </div>
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong> @lang('general.error_message')</strong><br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="create-videos-photos">
                <div class="page-header">
                    <h4>Adding photos to the video</h4>
                </div>
                <form id="video-photos-form" class="form-inline" action="{{url('videosphotos/'.$video_id.'/store')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{method_field('post')}}
                    <div class="control-group col-sm-12 separate-group">
                        <div class="col-sm-2">
                            <label for="photo-category" class="control-label">Belongs to</label>
                        </div>
                        <div class="col-sm-2">
                            <select name="photo_category" id="photo-category" class="span12">
                                <option value=""></option>
                                <option value="Video">Video</option>
                                <option value="Making off">Making off</option>
                            </select>
                        </div>

                        <div class="col-sm-2 col-sm-offset-1">
                            <label for="video-photo" class="control-label">Photo</label>
                        </div>
                        <div class="col-sm-2 {{ $errors->has('video_photo') ? 'has-error' : '' }}">
                            <div class="upload">
                                <a href="#" class="btn btn-default span12" role="button">
                                    <span class="glyphicon glyphicon-upload"></span>Select an image.
                                </a>
                            </div>
                            <input name="video_photo" id="video-photo" class="span12" type="file" value="" size="2048" required>
                            <input type="hidden" name="video_id" value="{{$video_id}}">
                        </div>
                        <div class="col-sm-2 col-sm-offset-1 along-button">
                            <button type="submit" id="create-video-photos" class="btn btn-success">Add</button>
                        </div>
                    </div>
                </form>
            </div>


            <!--Edit photo category-->
            <div class="videos-photos-edit">
                <div class="page-header">
                    <h4>Editing the selected photo</h4>
                </div>
                <form id="video-photose-form" class="form-inline " action="{{ url('videosphotos/update') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="control-group col-sm-12 separate-group">
                        <div class="col-sm-2">
                            <label for="photo-category-edit" class="control-label">Belongs to</label>
                        </div>
                        <div class="col-sm-2">
                            <select name="photo_category_edit" id="photo-category-edit" class="span12">
                                <option value=""></option>
                                <option id="video-option" value="Video">Video</option>
                                <option id="making-off-option" value="Making off">Making off</option>
                            </select>
                        </div>

                        <div class="col-sm-2 col-sm-offset-1">
                            <label for="video-photo-edit" class="control-label">Photo</label>
                        </div>
                        <div class="col-sm-2 {{ $errors->has('video_photo_edit') ? 'has-error' : '' }}">
                            <div class="upload">
                                <a href="#" class="btn btn-default span12" role="button">
                                    <span class="glyphicon glyphicon-upload"></span>Select an image.
                                </a>
                            </div>
                            <input name="video_photo_edit" id="video-photo-edit" size="2048" class="span12" type="file" value="">
                        </div>
                        <div class="col-sm-2 col-sm-offset-1 along-button">
                            <button type="submit" id="edit-video-photos" class="btn btn-success">Edit</button>
                            <button type="button" id="cedit-video-photos" class="btn btn-default">Cancel</button>
                        </div>
                    </div>
                    <input type="hidden" id="edit_id" name="edit_id">
                    <input type="hidden" name="video_id" value="{{$video_id}}">
                </form>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <div class="panel-body">
                        <p id="message-photo"></p>
                        <table id="table_videos" class="table" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Category</th>
                                <th>Photos</th>
                                <th>Actions</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $counter = 0; if($videophotos == '[]')$videophoto = new App\Videophoto();?>
                            @foreach($videophotos as $videophoto)
                                @if($videophoto->video_id == $video_id)
                                    <tr id="{{$videophoto->id}}">
                                        <td><?php echo ++$counter;?></td>
                                        <td>{{$videophoto->category}}</td>
                                        <td><a href="#see-video-image" role="button" data-toggle="modal" data-imageurl="{{$videophoto->photo}}" data-urlini="<?php public_path();?>" class="btn btn-xs btn-default">See photo</a></td>
                                        <td class="actions">
                                            {{--<a href="" data-ident="{{$videophoto->id}}" data-url="{{url('videosphotos/'.$videophoto->id)}}" id="photo-edit" class="btn btn-xs btn-success">Edit</a>--}}
                                            <button class="btn btn-xs btn-success" onclick="fun_edit('{{$videophoto->id}}')">Edit</button>
                                        </td>
                                        <td class="actions">
                                            <button type="button" data-target="#confirmation-message-deletev" data-toggle="modal" class="btn btn-xs btn-danger" data-ident="{{$videophoto->id}}" data-token="{{ csrf_token() }}" data-url="{{url('videosphotos/'.$videophoto->id)}}">Delete</button>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                        <input type="hidden" name="hidden_view" id="hidden_view" value="{{url('videosphotos/show')}}">
                    </div>
                </div>
            </div>
        </div>


    </div>


    <!-- Modal: confirmation message-->
    <div class="modal fade" id="confirmation-message-deletev" tabindex="-1" role="dialog" aria-labelledby="myModalLabelv">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelv">Confirmation message</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this photo?</p>
                </div>
                <div class="modal-footer">
                    {{csrf_field()}}
                    {{method_field('delete')}}
                    <button type="button" class="btn btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-sm button-cancel" id="delete-confirmv">Delete</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal: see image-->
    <div class="modal fade" id="see-video-image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <img src="" alt="" class="modal-image">
            </div>
        </div>
    </div>


    <!-- Edit Modal start -->
    {{--<div class="modal fade" id="editModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ url('videosphotos/update') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="form-group">
                                <label for="photo-category-edit">Belongs to:</label>
                                <select name="photo_category_edit" id="photo-category-edit" class="">
                                    <option value=""></option>
                                    <option id="video-option" value="Video">Video</option>
                                    <option id="making-off-option" value="Making off">Making off</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="video-photo-edit">Photo:</label>
                                <div class="upload">
                                    <a href="#" class="btn btn-default" role="button">
                                        <span class="glyphicon glyphicon-upload"></span>Select an image.
                                    </a>
                                </div>
                                <input name="video_photo_edit" id="video-photo-edit" class="form-control" type="file" value="">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-default">Update</button>
                        <input type="hidden" id="edit_id" name="edit_id">
                        <input type="hidden" name="video_id" value="{{$video_id}}">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
    </div>--}}
    <!-- Edit code ends -->


@endsection


