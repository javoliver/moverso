<div class="col-md-12 create-news-form">
    <div class="page-header">
        <h4>Editing the information of the selected element</h4>
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong> @lang('general.error_message')</strong><br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form class="form-inline" action="/whatwedo/{{$whatwedos->id}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{method_field('PUT')}}

        <div class="control-group">
            <label for="whatwedo-description-es" class="control-label">Description</label>
        </div>
        <div class="control-group whatwedo-group {{ $errors->has('spanish_description') ? 'has-error' : '' }}">
            <textarea id="whatwedo-description-es" name="spanish_description" rows="3" class="span12" required>{{$whatwedos->description_es}}</textarea>
            <p class="characterses"></p>
        </div>

        <div class="control-group">
            <label for="whatwedo-description-en" class="control-label">Description</label>
        </div>
        <div class="control-group whatwedo-group {{ $errors->has('english_description') ? 'has-error' : '' }}">
            <textarea id="whatwedo-description-en" name="english_description" rows="3" class="span12" required>{{$whatwedos->description_en}}</textarea>
            <p class="charactersen"></p>
        </div>

        <div class="control-group col-sm-12 my-buttons">
            <a href="{{url('/whatwedo')}}">
                <button type="button" class="btn btn-default">Cancel</button>
            </a>
            <button type="submit" class="btn btn-success">Edit</button>
        </div>
    </form>
</div>
