@extends('layouts.app')

@section('content')
    <div class="container news-page">
        <div class="row">
            <div class="col-md-12">
                <div class="table-header">
                    <h4>Managing "What we do" information</h4>
                    <p id="message-whatwedo"></p>
                </div>
                @if($whatwedos == [])
                    @include('whatwedo.create')
                @else
                    @include('whatwedo.edit')
                @endif

            </div>
        </div>
    </div>


@endsection
