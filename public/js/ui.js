//Initializing Datatables

$(document).ready(function() {
    $('#table_news').DataTable();
} );

$(document).ready(function() {
    $('#table_videos').DataTable();
} );

$(document).ready(function() {
    $('#table_whatwedo').DataTable();
} );

$(document).ready(function() {
    $('#table_member').DataTable();
} );

$(document).ready(function() {
    $('#table_artists').DataTable();
} );

$(document).ready(function() {
    $('#table_profession').DataTable();
} );

$(document).ready(function() {
    $('#table_role').DataTable();
} );

//Delete a news
$('#confirmation-message-delete').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('ident'); // Extract info from data-* attributes
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message');
    modal.find('#delete-confirm').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            method: "POST",
            url: 'news/' + id,
            data: { _token: $("input[name = '_token']").val(), _method: "delete" },
            success: function (response) {
                $('#' + id).remove();
                location.reload();
            },
            error: function (response) {
                console.log('Error while deleting.');
            }
        });
    })
});

//Delete a video
$('#video-confirmation-message-delete').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('identif'); // Extract info from data-* attributes
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message1');
    modal.find('#video-delete-confirm').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            method: "POST",
            url: 'videos/' + id,
            data: { _token: $("input[name = '_token']").val(), _method: "delete" },
            success: function (response) {
                $('#' + id).remove();
                location.reload();
            },
            error: function (response) {
                console.log('Error while deleting.');
            }
        });
    })
});

//Delete a personality
$('#confirmation-message-delete-artist').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('ident'); // Extract info from data-* attributes
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message-artist');
    modal.find('#delete-artist-confirm').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            method: "POST",
            url: 'personalities/' + id,
            data: { _token: $("input[name = '_token']").val(), _method: "delete" },
            success: function (response) {
                $('#' + id).remove();
                location.reload();
            },
            error: function (response) {
                console.log('Error while deleting.');
            }
        });
    })
});

//Delete a member
$('#confirmation-message-delete-member').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('ident'); // Extract info from data-* attributes
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message-member');
    modal.find('#delete-member-confirm').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            method: "POST",
            url: 'team/' + id,
            data: { _token: $("input[name = '_token']").val(), _method: "delete" },
            success: function (response) {
                $('#' + id).remove();
                location.reload();
            },
            error: function (response) {
                console.log('Error while deleting.');
            }
        });
    })
});


//Delete an element of the "What we do" section
$('#confirmation-message-delete-whatwedo').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('ident'); // Extract info from data-* attributes
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message-whatwedo');
    modal.find('#delete-whatwedo-confirm').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            method: "POST",
            url: 'whatwedo/' + id,
            data: { _token: $("input[name = '_token']").val(), _method: "delete" },
            success: function (response) {
                $('#' + id).remove();
                location.reload();
            },
            error: function (response) {
                console.log('Error while deleting.');
            }
        });
    })
});

//Delete an element of the "Contact us" section
$('#confirmation-message-delete-contactus').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('ident'); // Extract info from data-* attributes
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message-contactus');
    modal.find('#delete-contactus-confirm').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            method: "POST",
            url: 'contactus/' + id,
            data: { _token: $("input[name = '_token']").val(), _method: "delete" },
            success: function (response) {
                $('#' + id).remove();
                location.reload();
            },
            error: function (response) {
                console.log('Error while deleting.');
            }
        });
    })
});

//Delete a profession
$('#confirmation-message-delete-profession').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('ident'); // Extract info from data-* attributes
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message-profession');
    modal.find('#delete-profession-confirm').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            method: "POST",
            url: 'profession/' + id,
            data: { _token: $("input[name = '_token']").val(), _method: "delete" },
            success: function (response) {
                $('#' + id).remove();
               /* location.reload();*/
                /*$answer = JSON.stringify(response);
                alert($answer);*/
            },
            error: function (response) {
                errorMessage.text(response.responseText);
                errorMessage.addClass('alert alert-danger');
                $('#app').on('click', function () {
                    errorMessage.fadeOut(120);
                    location.reload();
                });
            }
        });
    })
});

//Delete a role
$('#confirmation-message-delete-role').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('ident'); // Extract info from data-* attributes
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message-role');
    modal.find('#delete-role-confirm').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            method: "POST",
            url: 'roles/' + id,
            data: { _token: $("input[name = '_token']").val(), _method: "delete" },
            success: function (response) {
                $('#' + id).remove();
            },
            error: function (response) {
                errorMessage.text(response.responseText);
                errorMessage.addClass('alert alert-danger');
                $('#app').on('click', function () {
                    errorMessage.fadeOut(120);
                    location.reload();
                });
            }
        });
    })
});


//Delete a photo
$('#confirmation-message-deletev').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var id = button.data('ident'); // Extract info from data-* attributes
    var token = button.data('token');
    var url = button.data('url');
    var modal = $(this);
    var page = $('.news-page');
    var errorMessage = page.find('#message-photo');
    modal.find('#delete-confirmv').on('click', function (e) {
        modal.hide();
        $('.modal-backdrop.in').hide();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                "id": id,
                "_method": 'DELETE',
                "_token": token
            },
            success: function ()
            {
                $('#' + id).remove();
            },
            error: function ()
            {
                console.log('Wrong');
               /* location.reload();*/
            }
        });
    });
});

//Load selected news description
$('#see-description').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var description = button.data('description'); // Extract info from data-* attributes
    $('#news-description-modal').text(description);
});


//Load selected artist summary
$('#see-artist-summary').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var summary = button.data('description'); // Extract info from data-* attributes
    $('#artist-summary-modal').text(summary);
});

//Load selected whatwedo description
$('#see-whatwedo-description').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var summary = button.data('description'); // Extract info from data-* attributes
    $('#whatwedo-description-modal').text(summary);
});


//Load selected news image and adjust de modal to the size of the image
$('#see-image').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var imageurl = 'storage/news-photos/' + button.data('imageurl'); // Extract info from data-* attributes
    var elemento = document.querySelector('.modal-image');
    elemento.setAttribute("src", imageurl);

    var modalDialogSize = $('#see-image .modal-dialog').width();
    var ModalImage= document.querySelector('.modal-image');
    var modalDialogSize = ModalImage.width;
    if(modalDialogSize > 1000){
        $('#see-image .modal-dialog').css('max-width', 1000);
    }else{
        $('#see-image .modal-dialog').css('max-width', '80%');
    }
    if($(window).width() <= 768){
        $('#see-image .modal-dialog').css('max-width', 'unset')
    }
});

//Load selected personality image and adjust de modal to the size of the image
$('#see-artist-image').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var imageurl = 'storage/personalities/' + button.data('imageurl'); // Extract info from data-* attributes
    var elemento = document.querySelector('.modal-image');
    elemento.setAttribute("src", imageurl);

    var modalDialogSize = $('#see-artist-image .modal-dialog').width;
    var ModalImage= document.querySelector('.modal-image');
    var modalDialogSize = ModalImage.width;
    if(modalDialogSize > 1000){
        $('#see-artist-image .modal-dialog').css('max-width', 1000);
    }else{
        $('#see-artist-image .modal-dialog').css('max-width', '80%');
    }
    if($(window).width() <= 768){
        $('#see-artist-image .modal-dialog').css('max-width', 'unset')
    }
});




//Load selected member image and adjust de modal to the size of the image
$('#see-member-image').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var imageurl = 'storage/team/' + button.data('imageurl'); // Extract info from data-* attributes
    var elemento = document.querySelector('.modal-image');
    elemento.setAttribute("src", imageurl);

    var modalDialogSize = 300;
    var ModalImage= document.querySelector('.modal-image');
    var modalDialogSize = ModalImage.width;
    if(modalDialogSize > 300){
        $('#see-member-image .modal-dialog').css('max-width', 300);
    }else{
        $('#see-member-image .modal-dialog').css('max-width', modalDialogSize);
    }
});


//Load selected video image and adjust de modal to the size of the image
$('#see-video-image').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var publicUrl = button.data('urlini');
    var imageurl = publicUrl + '/storage/video-photo/' + button.data('imageurl'); // Extract info from data-* attributes
    var elemento = document.querySelector('.modal-image');
    elemento.setAttribute("src", imageurl);

    var modalDialogSize = $(window).width();
    var ModalImage= document.querySelector('.modal-image');
    var modalDialogSize = ModalImage.width;
    if($(window).width() > 1536){
        $('#see-video-image .modal-dialog').css('max-width', 1000);
    }else{
        $('#see-video-image .modal-dialog').css('max-width', '80%');
    }
    if($(window).width() <= 768){
        $('#see-video-image .modal-dialog').css('max-width', 'unset')
    }
});




function fun_edit(id)
{
    var view_url = $("#hidden_view").val();
    $.ajax({
        url: view_url,
        type:"GET",
        data: {"id":id},
        success: function(result){
            //console.log(result);
            $("#edit_id").val(result.id);
            if(result.category == 'Video'){
                $('#video-option').attr('selected', 'selected');
            }
            if(result.category == 'Making off'){
                $('#making-off-option').attr('selected', 'selected');
            }
            $('.create-videos-photos').hide();
            $('.videos-photos-edit').show();
        }
    });
}


$('#cedit-video-photos').on('click', function (event) {
    $('.videos-photos-edit').hide();
    $('.create-videos-photos').show();
    location.reload();
});




$(function(){
    window.prettyPrint && prettyPrint();
    var startDate = new Date();
    $('#news-premiere_date').datepicker({
        startDate: startDate
    });
});


function InvalidMsg(textbox) {
    if (textbox.value == '') {
        textbox.setCustomValidity('This field is required');
    }
    else if (textbox.validity.typeMismatch) {
            textbox.setCustomValidity('Please enter a valid value');
        }
    else
        {
            textbox.setCustomValidity('');
        }
    return true;
}

$(document).ready(function (){
    var defaultRadio = $('#default').attr('checked');
    var videoRadio = $('#video').attr('checked');
    if(defaultRadio == 'checked'){
        $('.video-link-div').css('display', 'none');
        $('.video-label').css('display', 'none');
        $('.video-link-div').css('margin-bottom', '0px');
    }
    if(videoRadio == 'checked'){
        $('.video-link-div').css('display', 'inline');
        $('.video-link-div').css('margin-bottom', '20px');
        $('.video-label').css('display', 'inline');
    }
});


$('#video').on('click', function (e) {
    $('.video-link-div').css('display', 'inline');
    $('.video-label').css('display', 'inline');
});
$('#default').on('click', function (e) {
    $('.video-link-div').css('display', 'none');
    $('.video-label').css('display', 'none');
});

$('#edit-video-photos').on('click', function () {
    $('#edit-video-photos').css('display', 'inline');
});


$('#news-form').submit(function() {
    var fileSize = $('#news-photo')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);
    if (siezekiloByte >  $('#news-photo').attr('size')) {
        $('.my-message ').show();
        $('.my-message ').text('The image can not be bigger than 2 MB.');
        $('#app').on('click', function () {
            $('.my-message ').hide();
        });
        return false;
    }
});

$('#personality-form').submit(function() {
    var fileSize = $('#artist-photo')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);
    if (siezekiloByte >  $('#artist-photo').attr('size')) {
        $('.my-message ').show();
        $('.my-message ').text('The image can not be bigger than 2 MB.');
        $('#app').on('click', function () {
            $('.my-message ').hide();
        });
        return false;
    }
});

$('#member-form').submit(function() {
    var fileSize = $('#member-photo')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);
    if (siezekiloByte >  $('#member-photo').attr('size')) {
        $('.my-message ').show();
        $('.my-message ').text('The image can not be bigger than 2 MB.');
        $('#app').on('click', function () {
            $('.my-message ').hide();
        });
        return false;
    }
});

$('#video-photos-form').submit(function() {
    var fileSize = $('#video-photo')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);
    if (siezekiloByte >  $('#video-photo').attr('size')) {
        $('.my-message ').show();
        $('.my-message ').text('The image can not be bigger than 2 MB.');
        $('#app').on('click', function () {
            $('.my-message ').hide();
        });
        return false;
    }
});

$('#video-photose-form').submit(function() {
    var fileSize = $('#video-photo-edit')[0].files[0].size;
    var siezekiloByte = parseInt(fileSize / 1024);
    if (siezekiloByte >  $('#video-photo-edit').attr('size')) {
        $('.my-message ').show();
        $('.my-message ').text('The image can not be bigger than 2 MB.');
        $('#app').on('click', function () {
            $('.my-message ').hide();
        });
        return false;
    }
});




function CharactersAmount(element, charactersLength, messageElement) {
    element.keyup(function () {
        var tlength = $(this).val().length;
        $(this).val($(this).val().substring(0, charactersLength));
        var tlength = $(this).val().length;
        remain = charactersLength - parseInt(tlength);
        messageElement.text('Characters remaining '+remain);
    });
}

CharactersAmount($('#news-description'), 500, $('p.charactersSpanish'));
CharactersAmount($('#news-description-en'), 500, $('p.charactersEnglish'));
CharactersAmount($('#en'), 368, $('p.charactersPers'));
CharactersAmount($('#whatwedo-description-es'), 500, $('p.characterses'));
CharactersAmount($('#whatwedo-description-en'), 500, $('p.charactersen'));










