var browsers = ["Chrome", "Firefox", "Safari", "Opera", "MSIE"];
var b, userAgent = navigator.userAgent;
for(var i =0; i < browsers.length; i++){
    if(userAgent.indexOf(browsers[i]) > -1){
        b = browsers[i];
        break;
    }
}

var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var version = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));

/*IE Fix for "jumpy" fixed background*/
if(navigator.userAgent.match(/Trident\/7\./)) { // if IE
    $('body').on("mousewheel", function () {
        // remove default behavior
        event.preventDefault ? event.preventDefault() : (event.returnValue = false);

        //scroll without smoothing
        var wheelDelta = event.wheelDelta;
        var currentScrollPosition = window.pageYOffset;
        window.scrollTo(0, currentScrollPosition - wheelDelta);
    });
}



$('body').scrollspy({ target: '#navbar', offset: 56 });


function isScrollAfterElement(elem){
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();
    var elemTop = $elem.offset().top;

    return elemTop <= docViewBottom;
}



function resizePrimarySection() {
    var height = $(window).height(),
        width = $(window).width(),
        titleImageHeight = 141,
        universeTextHeight = 44,
        buttonsHeight = 56,
        rights = 15,
        titleImageProportion = 0,
        universeTextProportion = 0,
        buttonsProportion = 0,
        rightsProportion = 0,
        margin = 0;

    if(width >= 992){
        var rest = height - (titleImageHeight + universeTextHeight + buttonsHeight);
        rest = rest - 30;

        titleImageProportion = rest*0.3;
        universeTextProportion = rest*0.3;
        buttonsProportion = rest*0.25;
    }else{
        var rest = height - (titleImageHeight + universeTextHeight + buttonsHeight + rights);
        rest = rest - 30;

        titleImageProportion = rest*0.2;
        universeTextProportion = rest*0.2;
        buttonsProportion = rest*0.2;
        rightsProportion = rest*0.1;
    }

    if(height <= 480){
        rightsProportion = 0;
    }

    if(width < 768){
        $('#portfolio-content ul li.thumbnail-inner').css('width', width);
    }else if(width >=768 && width < 1200){
        $('#portfolio-content ul li.thumbnail-inner').css('width', width/2);
    }else if(width >=1200){
        $('#portfolio-content ul li.thumbnail-inner').css({'width': (width/3)+3, 'margin-left': '-1px'});
    }

    

    $('#main-view .title-image').css({'margin-top': titleImageProportion});
    $('.cover-heading').css({'margin-top': universeTextProportion, 'margin-bottom': buttonsProportion});
    $('.rights').css('margin-top', rightsProportion);

    $('.background1').css('height', height);
    $('#main-view').css('height', height);
    $('.cover-container').css('height', height);


    $('#what-we-do').css('width', width);
    $('#recent-work .videos-header-xs ul').css('width', width);
    $('#portfolio .portfolio-sub-header').css('width', width);
    $('.navbar-header').css('width', width);


    if(width < 1200){
        $('.services-list').css('width', width);
    }else{
        $('.services-list').css('width', 1198);
    }
    if(width < 1200){
        $('#what-we-do .whatwedo-filter').css('width', 'auto');
    }else if(width >= 768 && width <991){
        $('#what-we-do .whatwedo-filter').css('width', '100%');
    }
    if(width >= 992){
        $('.videos-header.first-one').css('width', width);
    }

    $('#navbar').css('width', width);

    if(width <= 991){
        $('#portfolio .whatwedo-filter').css('width', width);
        $('#our-team .perpendicular').css('width', width);
        if($('.navbar-default').css('min-height') != '0px'){
            $('nav.navbar').css({'min-height': 45, 'height': 45});
        }
    }else{
        $('#portfolio .whatwedo-filter').css('width', '100%');
        $('#our-team .perpendicular').css('width', 'auto');
    }

    if(width >= 320 && width <= 767){
        $('.container-fluid > .navbar-collapse').css('margin-left', 0);
    }

    if(width >= 768 && width <= 992){
        $('.container-fluid > .navbar-collapse').css('margin-left', '-15px');
    }

    if(width > 991 && width < 1200){
        if($('.navbar-default').css('min-height') != '0px'){
            $('nav.navbar').css({'min-height': 69, 'height': 69});
        }
    }

    if(width >= 1200){
        if($('.navbar-default').css('min-height') != '0px'){
            $('nav.navbar').css({'min-height': 55, 'height': 55});
        }
    }

    if($('.navbar-right').hasClass('unactive')){
        $('.gray-line').addClass('unactive');
        $('.menu-header').addClass('unactive');
    }else{
        $('.gray-line').removeClass('unactive');
        $('.menu-header').removeClass('unactive');
    }

    if($('.navbar-toggle').hasClass('collapsed') && width < 992){
        $('.navbar-right').addClass('unactive');
    }
}

$(window).ready(function () {
    resizePrimarySection();

    var windowWidth = $(window).width();
    var windowHeight = $(window).height();

    if (windowWidth <1200){
        displayNumbersOfThumbnail(8);
        displayNThumbnail(8);
    }else if(windowWidth >=1200){
        displayNumbersOfThumbnail(9);
        displayNThumbnail(9);
    }

    if(windowHeight <= 411){
        $('#navbar').css({'height': windowHeight, 'overflow-y': 'scroll'});
    }
});

$(window).resize(function () {
    resizePrimarySection();
    var windowWidth = $(window).width(), windowHeight = $(window).height();

    if (windowWidth <1200){
        displayNumbersOfThumbnail(8);
        displayNThumbnail(8);
    }else if(windowWidth >=1200){
        displayNumbersOfThumbnail(9);
        displayNThumbnail(9);
    }

    if(windowWidth <= 767){
        $('.navbar-header').css('margin-left', 0);
    }
    if(windowWidth > 767 && windowWidth < 992){
        $('.navbar-header').css('margin-left', '-15px');
    }

    if(windowHeight <= 411){
        $('#navbar').css({'height': windowHeight, 'overflow-y': 'scroll'});
    }
});


//Video carousel
$('#videoCarousel').carousel({
    interval: 10000
});

$('.thumbnailCarousel').each(function () {
    var rnd = Math.floor((Math.random() * 9000) + 1000);
    $(this).carousel({interval: rnd});
});

$('#newsCarousel').carousel({
    interval: 5000
});


$('#personalityCarousel').carousel({
    interval: 4550
});

$('#memberCarousel').carousel({
    interval: 5000
});

$('#memberCarousel1').carousel({
    interval: 5000
});

jQuery(document).ready(function() {
    jQuery("time.timeago").timeago();
});

//get the photos of a video
function fun_video_photos(id)
{
        var view_url = $("#hidden_view").val();
        $.ajax({
            url: view_url,
            type:"GET",
            data: {"id":id},
            success: function(result){
                $('#see-image-gallery .modal-content').append('<div id="allVideoPhotosCarousel" class="carousel slide">');
                $('#see-image-gallery #allVideoPhotosCarousel').append('<div class="carousel-inner">');
                for(var i = 0; i < result.length; i++){
                    if(i == 0){
                        $('#see-image-gallery .carousel-inner').append('<div class="item active"><img src="storage/video-photo/'+result[i].photo+'" alt="" class="modal-image allVideoPhotosCarouselimg"></div>')
                        continue;
                    }
                    $('#see-image-gallery .carousel-inner').append('<div class="item"><img src="storage/video-photo/'+result[i].photo+'" alt="" class="modal-image allVideoPhotosCarouselimg"></div>')

                }
                $('#see-image-gallery .modal-content').append('<a class="carousel-control left" href="#allVideoPhotosCarousel" data-slide="prev"><img src="img/home/back-angular.png" alt=""></a>');
                $('#see-image-gallery .modal-content').append('<a class="carousel-control right" href="#allVideoPhotosCarousel" data-slide="next"><img src="img/home/next-angular.png" alt=""></a>');
                $('#see-image-gallery .modal-content').append('</div></div>');


            }
        });

    $("#see-image-gallery").on("hidden.bs.modal", function() {
        $("#see-image-gallery .modal-content #allVideoPhotosCarousel").detach();
    });
}




function loadVideoModal() {
    $('#see-video-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var videoUrl = button.data('url');
        var elemento = document.querySelector('.embed-responsive-item');
        elemento.setAttribute("src", videoUrl);
    });
}

//To stop the video when the modal is closed
$('#see-video-modal').on('hidden.bs.modal', function (e) {
    var $if = $(e.delegateTarget).find('iframe');
    var src = $if.attr('src');
    $if.attr("src", '/empty.html');
    $if.attr("src", '');
});

//Filter ALL
$('.thumbnail-all').click(function () {
    $('.thumbnail-all').addClass('active-filter');
    $('#portfolio-direction-category').removeClass('active-filter');
    $('#portfolio-photography-category').removeClass('active-filter');
    $('#portfolio-production-category').removeClass('active-filter');
    $('#portfolio-postproduction-category').removeClass('active-filter');
    $('#portfolio .middle-block').addClass('hidden-xs');
    $('li.thumbnail-inner.liactive').each(function () {
        $(this).removeClass('unactive');
    })
});


//Filter DIRECTION
$('#portfolio-direction-category').click(function () {
    $('.thumbnail-all').removeClass('active-filter');
    $('#portfolio-direction-category').addClass('active-filter');
    $('#portfolio-photography-category').removeClass('active-filter');
    $('#portfolio-production-category').removeClass('active-filter');
    $('#portfolio-postproduction-category').removeClass('active-filter');
    $('#portfolio .middle-block').addClass('hidden-xs');
    $('li.liactive').each(function () {
        if($(this).hasClass('Direction')){
            $(this).removeClass('unactive');
        }else{
            $(this).addClass('unactive');
        }
    });
});


//Filter PHOTOGRAPHY
$('#portfolio-photography-category').click(function () {
    $('.thumbnail-all').removeClass('active-filter');
    $('#portfolio-photography-category').addClass('active-filter');
    $('#portfolio-direction-category').removeClass('active-filter');
    $('#portfolio-production-category').removeClass('active-filter');
    $('#portfolio-postproduction-category').removeClass('active-filter');
    $('#portfolio .middle-block').addClass('hidden-xs');
    $('li.liactive').each(function () {
        if($(this).hasClass('Photography')){
            $(this).removeClass('unactive');
        }else{
            $(this).addClass('unactive');
        }
    })
});


//Filter PRODUCTION
$('#portfolio-production-category').click(function () {
    $('.thumbnail-all').removeClass('active-filter');
    $('#portfolio-production-category').addClass('active-filter');
    $('#portfolio-direction-category').removeClass('active-filter');
    $('#portfolio-photography-category').removeClass('active-filter');
    $('#portfolio-postproduction-category').removeClass('active-filter');
    $('#portfolio .middle-block').addClass('hidden-xs');
    $('li.liactive').each(function () {
        if($(this).hasClass('Production')){
            $(this).removeClass('unactive');
        }else{
            $(this).addClass('unactive');
        }
    })
});

//Filter POSTRODUCTION
$('#portfolio-postproduction-category').click(function () {
    $('.thumbnail-all').removeClass('active-filter');
    $('#portfolio-postproduction-category').addClass('active-filter');
    $('#portfolio-direction-category').removeClass('active-filter');
    $('#portfolio-photography-category').removeClass('active-filter');
    $('#portfolio-production-category').removeClass('active-filter');
    $('#portfolio .middle-block').addClass('hidden-xs');
    $('li.liactive').each(function () {
        if($(this).hasClass('PostProduction')){
            $(this).removeClass('unactive');
        }else{
            $(this).addClass('unactive');
        }
    })
});


//Thumbnail hover
$('li.thumbnail-inner').each(function () {
    var id = '.' + $(this).data('id');
   $(this).bind('mouseenter mouseleave', function () {
       $(id).toggleClass('unactive');
       $('.sharebuttons').addClass('unactive');
       removeColor();
    });
});

//Thumbnail links hover
$('.links1').bind('mouseenter mouseleave', function () {
    $('.my-line1').toggleClass('orange-line');
    $('.my-line1').toggleClass('under-line');
});
$('.links2').bind('mouseenter mouseleave', function () {
    $('.my-line2').toggleClass('orange-line');
    $('.my-line2').toggleClass('under-line');
});

$('.up').on('click', function (e) {
    event.preventDefault ? event.preventDefault() : (event.returnValue = false);
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
});



$('.news-carousel .item').each(function () {
    var premiere = $(this).data('premiere');
    var id = $(this).data('id');
    var seeMore = $(this).data('seemore');
    var countDownDate = new Date(premiere).getTime();

    if(seeMore == ""){
        $('#see-more-'+id).css('opacity', 0)
    }else{
        $('#see-more-'+id).css('opacity', 1)
    }

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds for the days left for premiere
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);


        // Output the result in an element with id="demo"
        if(days > 0){
            document.getElementById("counter-"+id).innerHTML = days;
        }else{
            var url = document.getElementById("counter-"+id);
            $('.class-'+id+' '+'#timer-image').remove();
            $('.class-'+id+' '+' span').remove();
            $('#play-video-'+id).removeClass('unactive');
        }

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
        }
    }, 1000);
});



function displayNumbersOfThumbnail (number){
    var thumbnailCount = 0;

    $('li.thumbnail-inner').each(function () {
        $(this).removeClass('unactive');
        $(this).addClass('liactive');
        if(thumbnailCount >= number){
            $(this).addClass('unactive');
            $(this).removeClass('liactive');
        }
        thumbnailCount++;
    });
}



//To display the thumbnail 9 by 9, the rest of the images doesn´t display
function displayNThumbnail (number) {
    var aux = 0;
    var clicks = 1;
    $('#see-more-thumbnail').click(function () {
        clicks++;
        thumbnailCount = 0;
        var count = $('#video-count').data('count');
        var aux = count/number;
        var aux2 = count%number;
        $('li.thumbnail-inner').each(function () {
            $(this).addClass('liactive');
            $(this).removeClass('unactive');
            if(thumbnailCount >= number*clicks){
                $(this).removeClass('liactive');
                $(this).addClass('unactive');
                if(thumbnailCount == number*clicks){
                    return;
                }
            }
            thumbnailCount++;
        });
        if(thumbnailCount == count){
            $('#see-more-thumbnail').addClass('gray')
        }
    });
}




$('.display-thumbnail-filter').click(function () {
    $('#portfolio .middle-block').toggleClass('hidden-xs');
});

$('#portfolio-content').click(function () {
    $('#portfolio .middle-block').addClass('hidden-xs');
});


$('#recent-work img.share').click(function () {
    var width = $(window).width();
    removeColor();
    $('#recent-work .sharebuttons').toggleClass('unactive');
    if(width < 992){
        $('#recent-work .carousel-control.left').addClass('unactive');
    }
});

$('#portfolio img.share').click(function () {
    removeColor();
    $('#portfolio .sharebuttons').toggleClass('unactive');
});

$('#news img.share').click(function () {
    var width = $(window).width();
    removeColor();
    $('#news .sharebuttons').toggleClass('unactive');
    $('#news .carousel-control.left').addClass('unactive');
});


//For the sharebuttons styles when clicking
$('.sharebuttons ul li a').click(function () {
    var width = $(window).width();
    removeColor();
    $(this).css('color', '#051D49');
    $(this).siblings('.down-line').css('background-color', '#fc6c0c');
    $('.sharebuttons').addClass('unactive');
    if(width < 992){
        $('#recent-work .carousel-control.left').removeClass('unactive');        
    }  
    $('#news .carousel-control.left').removeClass('unactive');  
});

function removeColor() {
    $('.sharebuttons ul li a').css('color', '#969696');
    $('.share-elements .down-line').css('background-color', '#969696');
}


//To close the .sharebuttons element when clicking outside
$('*').click(function(e) {
    var target = $(e.target);
    var width = $(window).width();    
    if(!$(target).is('img.share') && !$(target).is('.sharebuttons li a')) {
        removeColor();
        $('.sharebuttons').addClass('unactive');
        if(width < 992){
            $('#recent-work .carousel-control.left').removeClass('unactive');            
        }
        $('#news .carousel-control.left').removeClass('unactive');
    }
});

$(window).scroll(function(){
    var width = $(window).width(); 
    if(width < 992 && ($('#recent-work .sharebuttons').hasClass('unactive'))){
        $('#recent-work .carousel-control.left').removeClass('unactive');
    }
    if(width < 992 && ($('#news .sharebuttons').hasClass('unactive'))){
        $('#news .carousel-control.left').removeClass('unactive');
    }
});



$('#email-subject').click(function () {
    $('.subject-options').toggleClass('unactive');
    var pobe = $('#email-subject').width();
    $('.subject-options').css('width', pobe);
});

$('*').click(function(e) {
    var target = $(e.target);
    if(!$(target).is('#email-subject')) {
        $('.subject-options').addClass('unactive');
    }
});

$('.subject-options ul li').click(function (event) {
    var value = $(this).data('value');
    $('#email-subject').attr('value', value);  
});









/*var lang = window.navigator.userLanguage || window.navigator.language;
alert(lang);*/






//To smooth scroll <a>
$('.smoothScroll').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            value = target.offset().top - 55;
            $('html,body').animate({
                scrollTop: value
            }, 1000); // The number here represents the speed of the scroll in milliseconds
            return false;
        }
    }
});


(function($) {
    //Global variables
    var star1 = $('.star'),
        titleImage = $('.title-image'),
        coverHeading = $('.cover-heading'),
        menu = $('.menu'),
        menuWord = $('.menu p'),
        mainButtons = $('.main-buttons'),
        orangeContainer = $('#orange-container'),
        movementImage = document.getElementById("stars5"),
        primarySection = $('.pimary-section'),
        coverContainer = $('.cover-container, .star'),
        navbarBrand = $('.navbar-brand'),
        welcomeImage = $('.welcome-image'),
        once = 0,
        uno = 0,
        rights = $('.rights'),
        tl1 = new TimelineLite(),
        tl2 = new TimelineLite(),
        tl3 = new TimelineLite(),
        tl4 = new TimelineLite();


    var mainAnimation = function () {
        tl1.staggerFromTo(star1, 0.4,
            {cycle: {x: [50, -50], scale: [2, 0.5]}, autoAlpha: 0, ease:Power1.easeOut, onComplete: moveStars},
            {x: 0, scale: [1, 1], autoAlpha: 1, ease:Power1.easeOut},
            0.1)
            .to(titleImage, 0.8, {autoAlpha: 1, ease:Power1.easeOut}, '-=0.2')
            .to(coverHeading, 0.8, {autoAlpha: 1, ease:Power1.easeOut}, '-=0.2')
            .to(mainButtons, 0.8, {autoAlpha: 1, ease:Power1.easeOut}, '-=0.2')
            .to(menu, 0.3, {autoAlpha: 1, ease:Power1.easeOut}, '-=0.3')
            .to(rights, 0.3, {autoAlpha: 1, ease:Power1.easeOut}, '-=0.3')
            .to($('.navbar-default .navbar-toggle'), 0.3, {autoAlpha: 1, ease:Power1.easeOut}, '-=0.3')
    };

    window.requestAnimFrame = (function(){
      return  window.requestAnimationFrame       ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame    ||
              function( callback ){
                window.setTimeout(callback, 1000 / 60);
              };
    })();


    var everythingLoaded = setInterval(function() {
        if (/loaded|complete/.test(document.readyState)) {
            clearInterval(everythingLoaded);
            // this is the function that gets called when everything is loaded
            requestAnimFrame(mainAnimation);
        }
    }, 10);

    $('.services-list > li').css('opacity', 0);

    //Functions
    function moveStars() {
        TweenMax.to(movementImage, 8, {
            x: 15,
            z: 0.1, // use if jitter or shaking is really bad
            rotationZ: 0.01, // use if jitter or shaking is really bad
            ease: Power0.easeInOut,
            force3D:true,
            repeat: -1,
            yoyo: true
        });
    }
    function fixMenu() {
        var width = $(window).width();
        $('nav').css({'position' : 'fixed'});         
        $('.container-fluid > .navbar-header').css('margin-left', '-15px');
        $('nav').css({'left' : 0});
        $('.navbar-default .navbar-toggle').css('cursor', 'pointer');
    }

    function giveOpacity() {
        primarySection.css('opacity', 0);
    }

    function displaySections() {
        $('.row').removeClass('unactive');
        jQuery(window).trigger('resize').trigger('scroll');
    }

    function showNavAndOrangeContainer() {
        var width = $(window).width();
        if(width >= 992 && width < 1200){
            orangeContainer.css({'visivility': 'visible', 'height': 300});
            $('nav.navbar').css({'overflow': 'inherit', 'margin-top': 0, 'min-height': 69, 'height': 69});
        }
        if(width >= 1200){
            orangeContainer.css({'visivility': 'visible', 'height': 300});
            $('nav.navbar').css({'overflow': 'inherit', 'margin-top': 0, 'min-height': 55, 'height': 55});
        }
        if(width < 992){
            orangeContainer.css({'visivility': 'visible', 'height': 300});
            $('nav.navbar').css({'overflow': 'inherit', 'margin-top': 0, 'min-height': 45, 'height': 45});
        }
    }



    //Function for moving elements continously
    function continouslyAnimation(element, speed, position, repeat, continuity) {
        TweenMax.to(element, speed, {
            x: position,
            z: 0.1, // use if jitter or shaking is really bad
            rotationZ: 0.01, // use if jitter or shaking is really bad
            ease: Power0.easeNone,
            force3D:true,
            repeat: repeat,
            yoyo: continuity
        });
    }

    //Function for animating services elements
    function AnimateServiceElements(){
        var docVieT = $(window).scrollTop(),
            recentTop= $("#services").offset().top - 200;

        if( docVieT >= recentTop && once == 0) {
            tl3.staggerFromTo($('.services-list > li:nth-child(1)'), 0.1, {autoAlpha: 0, scale: 0, ease: Power0.easeOut}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut}, 0.2)
                    .staggerFromTo($('.services-list > li:nth-child(2)'), 0.1, {autoAlpha: 0, scale: 0, ease: Power0.easeOut}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut}, '-=0.2')
                    .staggerFromTo($('.services-list > li:nth-child(3)'), 0.1, {autoAlpha: 0, scale: 0, ease: Power0.easeOut}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut}, '-=0.4')
                    .staggerFromTo($('.services-list > li:nth-child(4)'), 0.1, {autoAlpha: 0, scale: 0, ease: Power0.easeOut}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut}, '-=0.6')
                    .staggerFromTo($('.services-list > li:nth-child(5)'), 0.1, {autoAlpha: 0, scale: 0, ease: Power0.easeOut}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut}, '-=0.8')
                    .staggerFromTo($('.services-list > li:nth-child(6)'), 0.1, {autoAlpha: 0, scale: 0, ease: Power0.easeOut}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut}, '-=1')
                    .staggerFromTo($('.services-list > li:nth-child(7)'), 0.1, {autoAlpha: 0, scale: 0, ease: Power0.easeOut}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut}, '-=1.2')
                    .staggerFromTo($('.services-list > li:nth-child(8)'), 0.1, {autoAlpha: 0, scale: 0, ease: Power0.easeOut}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut}, '-=1.4');
            once = 1;
        }
    }


    //Function for animating member elements
    function AnimateMemberElements(){
        var docVieT = $(window).scrollTop(),
            memberTop= $("#our-team").offset().top - 200;
        var delayValue = 0.2;

        if( docVieT >= memberTop && docVieT != 0 && uno == 0){
            $('.member-one .member').each(function () {
                TweenMax.staggerFromTo($(this), 0.5,  {autoAlpha: 0, scale: 0, ease: Power0.easeOut, delay: delayValue}, {autoAlpha: 1, scale: 1, ease: Power0.easeOut, delay: delayValue});
                delayValue = delayValue + 0.1;
            });
            uno = 1;
        }
    }


    //Function for adjusting the images for different dimensions
    function adjustImage(){
        var liWidth = $('.thumbnail-inner').width(), Iheight = 0, Iwidth = 0,
            windowWidth = $(window).width();

        if(windowWidth > 1366){
            $('#recent-work .video-main-image').css({'width': '100%', 'height': 'auto', 'min-width': '100%', 'min-height': 'auto', 'top': 0});
            $('#news .news-img-carousel').css({'width': '100%', 'height': 'auto', 'min-width': '100%', 'min-height': 'auto', 'top': 0});
        }else{
            $('#recent-work .video-main-image').css({'width': 'auto', 'height': '100%', 'min-width': 'auto', 'min-height': '100%', 'left': '50%', 'transform': 'translate(-50%,0)', '-ms-transform': 'translate(-50%,0)', '-webkit-transform': 'translate(-50%,0)'});
            $('#news .news-img-carousel').css({'width': 'auto', 'height': '400px', 'min-width': 'auto', 'min-height': '400px', 'left': '50%', 'transform': 'translate(-50%,0)', '-ms-transform': 'translate(-50%,0)', '-webkit-transform': 'translate(-50%,0)'});
        }
    }


    //Function for determining the scroll width of browsers
    function getScrollBarWidth () {
        var inner = document.createElement('p');
        inner.style.width = "100%";
        inner.style.height = "200px";

        var outer = document.createElement('div');
        outer.style.position = "absolute";
        outer.style.top = "0px";
        outer.style.left = "0px";
        outer.style.visibility = "hidden";
        outer.style.width = "200px";
        outer.style.height = "150px";
        outer.style.overflow = "hidden";
        outer.appendChild (inner);

        document.body.appendChild (outer);
        var w1 = inner.offsetWidth;
        outer.style.overflow = 'scroll';
        var w2 = inner.offsetWidth;
        if (w1 == w2) w2 = outer.clientWidth;

        document.body.removeChild (outer);

        return (w1 - w2);
    };




    adjustImage();

    //Events handlers
    //Clicking the "See our work" button
    $('.see-our-work').click(function (event) {
        TweenMax.staggerFromTo($('.see-our-work'), 0.3, {scale: 1.1, ease:Power1.easeOut}, {scale: 1, ease:Power1.easeOut});        
        showNavAndOrangeContainer();
        displaySections();
        event.preventDefault ? event.preventDefault() : (event.returnValue = false);
        $('html, body').scrollTop(0);
        $(this).css({'background-color': '#ff6c0c', 'border-color': '#ff6c0c'});
        var width = $(window).width();
        var negativeHeight = $(window).height()*-1;
        

        if(width >= 992){
            if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1))
            {
              $('#main-view').css('left', '0px'); 
            }else{
                $('#main-view').css('left', '8px'); 

            }                      
        }

        tl2.to(menuWord, 0.7, {autoAlpha: 0, display: 'none', ease:Power0.easeOut}, 1)
            .to(coverContainer, 1, {top: negativeHeight}, 1)
            .to(primarySection, 0.7, {height: 0, overflow: "hidden", marginTop: "-2px", ease:Power0.easeOut, onComplete: giveOpacity}, 1)
            .to(orangeContainer, 1, {height: 0, ease:Power0.easeOut, onComplete: fixMenu}, 1)
            .to(navbarBrand, 1.5, {autoAlpha: 1, ease: Power0.easeOut}, 1.7)
            .to(welcomeImage, 1, {autoAlpha: 1, ease: Power0.easeOut}, 2.2);
        

        

        $('.menu').css('cursor', 'pointer');
    });

    //Clicking the menu image for displaying the menu options
    $('.menu').click(function () {
        $('ul.navbar-right').toggleClass('unactive');
        $('.gray-line').toggleClass('unactive');
    });


    //Clicking over the menu toggle for mobile versions
    $('.navbar-toggle').click(function (e) {
            $('.navbar-default .navbar-toggle .icon-bar').toggleClass('blue-color');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(3)').toggleClass('opacity');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(2)').toggleClass('rotate');
            $('.navbar-default .navbar-toggle .icon-bar:nth-child(4)').toggleClass('rotate-negative');
            $('.navbar-toggle').toggleClass('margin-aux');
    });


    //Actions that happens when scrolling
    $(window).scroll(function () {
        AnimateServiceElements();
        AnimateMemberElements();
    });


    //Click on the navbar-toogle buttons for displaying menu in mobile
    $('.navbar-toggle').click(function () {
        $('.navbar-right').removeClass('unactive');
    });

    //Clicking in a menu option in a mobile
    $('#navbar ul li a').click(function () {
        $('.navbar-toggle').addClass('collapsed');
        $('#navbar').removeClass('in');
        $('.navbar-default .navbar-toggle .icon-bar').removeClass('blue-color');
        $('.navbar-default .navbar-toggle .icon-bar:nth-child(3)').removeClass('opacity');
        $('.navbar-default .navbar-toggle .icon-bar:nth-child(2)').removeClass('rotate');
        $('.navbar-default .navbar-toggle .icon-bar:nth-child(4)').removeClass('rotate-negative');
        $('.navbar-toggle').removeClass('margin-aux');
    });



    //Function for moving lines when the mouse is over the carousel controls
    function animateArrows(control, element, distance) {
        control.hover(function() {
            TweenMax.to(element, 1, {
                x: distance,
                z: 0.1, // use if jitter or shaking is really bad
                rotationZ: 0.01, // use if jitter or shaking is really bad
                ease: Power0.easeNone,
                force3D:true,
                repeat: -1,
                yoyo: true
            })
        }, function() {
            TweenMax.to(element, 1, {
                x: 0,
                z: 0.1, // use if jitter or shaking is really bad
                rotationZ: 0.01, // use if jitter or shaking is really bad
                ease: Power0.easeOut,
                force3D:true
            })
        });
    }

    animateArrows($('#recent-work .carousel-control.left'), $('#recent-work .carousel-control.left > img:nth-child(2)'), -10);
    animateArrows($('#recent-work .carousel-control.left'), $('#recent-work .carousel-control.left > img:nth-child(3)'), 10);
    animateArrows($('#recent-work .carousel-control.right'), $('#recent-work .carousel-control.right > img:nth-child(2)'), 15);
    animateArrows($('#recent-work .carousel-control.right'), $('#recent-work .carousel-control.right > img:nth-child(3)'), -15);


    animateArrows($('#news .carousel-control.left'), $('#news .carousel-control.left > img:nth-child(2)'), -10);
    animateArrows($('#news .carousel-control.left'), $('#news .carousel-control.left > img:nth-child(3)'), 10);
    animateArrows($('#news .carousel-control.right'), $('#news .carousel-control.right > img:nth-child(2)'), 15);
    animateArrows($('#news .carousel-control.right'), $('#news .carousel-control.right > img:nth-child(3)'), -15);


    animateArrows($('#personality .carousel-control.left'), $('#personality .carousel-control.left > img:nth-child(2)'), -10);
    animateArrows($('#personality .carousel-control.left'), $('#personality .carousel-control.left > img:nth-child(3)'), 10);
    animateArrows($('#personality .carousel-control.right'), $('#personality .carousel-control.right > img:nth-child(2)'), 15);
    animateArrows($('#personality .carousel-control.right'), $('#personality .carousel-control.right > img:nth-child(3)'), -15);


    animateArrows($('#our-team .carousel-control.left'), $('#our-team .carousel-control.left > img:nth-child(2)'), -10);
    animateArrows($('#our-team .carousel-control.left'), $('#our-team .carousel-control.left > img:nth-child(3)'), 10);
    animateArrows($('#our-team .carousel-control.right'), $('#our-team .carousel-control.right > img:nth-child(2)'), 15);
    animateArrows($('#our-team .carousel-control.right'), $('#our-team .carousel-control.right > img:nth-child(3)'), -15);


    //Actions to do when de window resize
    $(window).resize(function () {
        var windowWidth = $(window).width();
        adjustImage();

        if(windowWidth > 991){
            $('.navbar-toggle').css('display', 'none');
        }
        if(windowWidth <=991){
            $('.navbar-toggle').css('display', 'inline-block');
        }
    });


    //Fixing the problem with placeholder in some browsers
    if (navigator.appName == 'Microsoft Internet Explorer' ||  !!(navigator.userAgent.match(/Trident/) || navigator.userAgent.match(/rv:11/)) || (typeof $.browser !== "undefined" && $.browser.msie == 1)){        

        if (version <=9){
            $('input, textarea').placeholder();
        }
        
    }
    
        

})(jQuery);


