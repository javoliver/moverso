<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;



Route::get('/', function () {
    return view('home');
})->middleware('language');



Auth::routes();

Route::get('/home', 'HomeController@index');


//Route for the admin index
Route::get('/admin', function (){
    if (Auth::check()){
        $configuration = DB::table('general-configuration')->where('identifier', 1)->first();
        if($configuration != NULL){
            return view('admin.index', ['configuration' => $configuration]);
        }else{
            $configuration = new \App\Configuration();
            return view('admin.index', ['configuration' => $configuration]);
        }
    }
    return redirect('login');

});

//Routes for the managing news resources
Route::resource('news', 'NewsController');

//Routes for the managing videos resources
Route::resource('videos', 'VideosController');

//Routes for the managing videos photos resources
/*Route::resource('videosphotos', 'VideophotosController');*/
Route::get('videosphotoss/{id}', function ($id){
    $error = '';
    if (Auth::check()){
        $videoPhotos = App\Videophoto::all();
        return view('videosphotos.index', ['videophotos' => $videoPhotos, 'error' => '', 'video_id' => $id]);
    }
    return redirect('login');
});

Route::post('videosphotos/{id}/store', 'VideophotosController@store');

Route::post('videosphotos/update', 'VideophotosController@update');
Route::get('videosphotos/show', 'VideophotosController@show');
Route::delete('videosphotos/{id}', 'VideophotosController@destroy');
Route::get('photosofavideo', 'VideophotosController@photosofavideo');

/*Route::resource('videosphotos', 'PhotosController');*/

//Routes for the managing personalities resources
Route::resource('personalities', 'PersonalitiesController');

//Routes for the team personalities resources
Route::resource('team', 'TeamController');

//Routes for the "What we do" resources
Route::resource('whatwedo', 'WhatwedoController');

//Routes for the "Contact us" resources
Route::resource('contactus', 'ContactusController');

//Routes for the profession resources
Route::resource('profession', 'ProfessionController');

//Routes for the role resources
Route::resource('roles', 'RoleController');

Route::get('prueba', function (){
    $member = DB::table('personalities')->where('profession_id', 9)->first();
   return view('prueba.index', ['member' => $member]);
});

//Route for general configuration
Route::resource('configuration', 'ConfigurationController');


Route::get('change-password', function() {return view('admin.change-password'); });
Route::post('change-password', 'Auth\UpdatePasswordController@update');


Route::post('send', ['as' => 'send', 'uses' => 'MailController@send'] );
Route::get('contact', ['as' => 'contact', 'uses' => 'MailController@index'] );

Route::post('contact-us', 'ContactusController@sendEmail');