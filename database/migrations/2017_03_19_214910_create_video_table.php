<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table){
            $table->increments('id');
            $table->string('title');
            $table->string('artists');
            $table->string('making_off_url')->nullable();
            $table->string('video_url')->nullable();
            $table->string('category');
            $table->integer('priority')->nullable();
            $table->unique('priority');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
