<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralConfigurationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general-configuration', function (Blueprint $table){
            $table->increments('id');
            $table->integer('identifier');
            $table->string('configuration');
            $table->string('video_url')->nullable();
            $table->string('seew_video_link')->nullable();
            $table->string('peak_video_link')->nullable();
            $table->boolean('welcome_image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general-configuration');
    }
}
