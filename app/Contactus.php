<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactus extends Model
{
    use \Dimsav\Translatable\Translatable;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_us';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email'
    ];

    /**
     * The attributes that are translatable
     */
    public $translatedAttributes = ['subject'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
