<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
 * The database table used by the model.
 *
 * @var string
 */
    protected $table = 'videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'artists', 'making_off_url','video_url', 'category', 'priority'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    // each Video has many photos
    public function videophotos() {
        return $this->hasMany('App\Videophoto', 'video_id');
    }
}
