<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = '';
        if (Auth::check()){
            $roles = Role::all();
            return view('roles.index', ['roles' => $roles]);
        }
        return redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'spanish_role' => 'required|unique:role|max:255',
            'english_role' => 'required|unique:role|max:255',
        ]);


        $roleToAdd = new Role();

        $roleToAdd->spanish_role = $request->spanish_role;
        $roleToAdd->english_role = $request->english_role;

        $roleToAdd->save();

        return redirect('roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        return view('roles.edit', ['role' => $role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $roleToEdit = Role::find($id);

        $this->validate($request, [
            'spanish_role' => 'required|max:255|unique:role,spanish_role,'.$roleToEdit->id,
            'english_role' => 'required|max:255|unique:role,english_role,'.$roleToEdit->id,
        ]);

        $roleToEdit->spanish_role = $request->spanish_role;
        $roleToEdit->english_role = $request->english_role;

        $roleToEdit->save();

        return redirect('roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $member = DB::table('member')->where('role_id', $id)->first();
        $roles = Role::all();

        $role = Role::find($id);
        $response = new Response();
        if($member != NULL){
            return response("Can not delete the role because it is related with a member.", Response::HTTP_INTERNAL_SERVER_ERROR);
        }else{
            $role->delete();
        }

        return response("Element deleted.", Response::HTTP_OK);
    }
}
