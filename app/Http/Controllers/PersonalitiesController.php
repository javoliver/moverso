<?php

namespace App\Http\Controllers;

use App\Artists;
use App\Personalities;
use App\Profession;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class PersonalitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = '';
        if (Auth::check()){
            $personalities = Personalities::all();
            return view('personalities.index', ['personalities' => $personalities]);
        }
        return redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $professions = Profession::all();
        return view('personalities.create', ['professions' => $professions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName = str_random(5).time().'.'.$request->file('artist_photo')->guessClientExtension();
        $fileNameMobile = str_random(5).time().'.'.$request->file('artist_mobile_photo')->guessClientExtension();

        if(isset($request->artist_name) && isset($request->artist_last_name)){
            $request->request->add(['my_key' => 'personalities-'.$request->artist_name.'-'.$request->artist_last_name.'-name-last_name']);
        }

        $this->validate($request, [
            'my_key' => 'required|uniqueTwoElements',
            'artist_profession' => 'required',
            'work_interpreters' => 'required',
            'artist_work' => 'required',
            'artist_summary' => 'required',
            'artist_summary_en' => 'required',
            'artist_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'artist_mobile_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);


        $path = $request->file('artist_photo')->storeAs(
            'personalities', $fileName, 'public'
        );

        $path = $request->file('artist_mobile_photo')->storeAs(
            'personalities', $fileNameMobile, 'public'
        );

        $personalityAdd = new Personalities();

        $personalityAdd->name = $request->artist_name;
        $personalityAdd->last_name = $request->artist_last_name;
        $personalityAdd->translateOrNew('es')->summary = $request->artist_summary;
        $personalityAdd->translateOrNew('en')->summary = $request->artist_summary_en;
        $personalityAdd->photo = $fileName;
        $personalityAdd->photo_mobile = $fileNameMobile;
        $personalityAdd->social_network_address = $request->artist_web_address;
        $personalityAdd->artist_work = $request->artist_work;
        $personalityAdd->work_interpreters = $request->work_interpreters;
        $personalityAdd->profession_id = $request->artist_profession;

        $personalityAdd->save();

        return redirect('personalities');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artist = Personalities::find($id);
        $professions = Profession::all();
        return view('personalities.edit', ['artist' => $artist, 'error' => '', 'professions' => $professions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $artistToEdit = Personalities::find($id);

        if(isset($request->artist_name) && isset($request->artist_last_name)){
            $request->request->add(['epmy_key' => 'personalities-'.$request->artist_name.'-'.$request->artist_last_name.'-'.$artistToEdit->id.'-name-last_name']);
        }

        $this->validate($request, [
            'epmy_key' => 'required|uniqueTwoElementsEdit',
            'artist_profession' => 'required',
            'work_interpreters' => 'required',
            'artist_work' => 'required',
            'artist_summary' => 'required',
            'artist_summary_en' => 'required',
            'artist_photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'artist_mobile_photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);


        //Falta verificar q esto funcione bien
        if($request->file('artist_photo')/*->getClientOriginalName()*/ !== NULL){
            if(file_exists(storage_path('app/public/personalities/'.$artistToEdit->photo))){
                unlink(storage_path('app/public/personalities/'.$artistToEdit->photo));
            }

            $fileName = str_random(5).time().'.'.$request->file('artist_photo')->guessClientExtension();

            $path = $request->file('artist_photo')->storeAs(
                'personalities', $fileName, 'public'
            );
            $photo = $fileName;
        }else{
            $photo = $artistToEdit->photo;
        }

        if($request->file('artist_mobile_photo')/*->getClientOriginalName()*/ !== NULL){
            if(file_exists(storage_path('app/public/personalities/'.$artistToEdit->photo_mobile))){
                unlink(storage_path('app/public/personalities/'.$artistToEdit->photo_mobile));
            }

            $fileNameMobile = str_random(5).time().'.'.$request->file('artist_mobile_photo')->guessClientExtension();

            $path = $request->file('artist_mobile_photo')->storeAs(
                'personalities', $fileNameMobile, 'public'
            );
            $photoMobile = $fileNameMobile;
        }else{
            $photoMobile = $artistToEdit->photo_mobile;
        }

        $artistToEdit->name = $request->artist_name;
        $artistToEdit->last_name = $request->artist_last_name;
        $artistToEdit->translate('es')->summary = $request->artist_summary;
        $artistToEdit->translate('en')->summary = $request->artist_summary_en;
        $artistToEdit->photo = $photo;
        $artistToEdit->photo = $photoMobile;
        $artistToEdit->social_network_address = $request->artist_web_address;
        $artistToEdit->artist_work = $request->artist_work;
        $artistToEdit->work_interpreters = $request->work_interpreters;
        $artistToEdit->profession_id = $request->artist_profession;

        $artistToEdit->save();
        return redirect('personalities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $artist = Personalities::find($id);
        /*Storage::delete(storage_path('app/public/personalities/'.$artist->photo)); It doesn't work*/
        if(file_exists(storage_path('app/public/personalities/'.$artist->photo))){
            unlink(storage_path('app/public/personalities/'.$artist->photo));
        }

        if(file_exists(storage_path('app/public/personalities/'.$artist->photo_mobile))){
            unlink(storage_path('app/public/personalities/'.$artist->photo_mobile));
        }

        $artist->delete();

        return response("Element deleted.", Response::HTTP_OK);
    }
}
