<?php

namespace App\Http\Controllers;

use App\Configuration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConfigurationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $getConfiguration = Configuration::find(1);
        $configuration = '';
        $videoUrl = '';


        if($getConfiguration != NULL){
            $getConfiguration->configuration = $request->optionsRadios;
            if($request->optionsRadios == 'video'){
                $getConfiguration->video_url = $request->video_link;
            }else{
                $getConfiguration->video_url = NULL;
            }
            if($request->welcome_image == 'true'){
                $getConfiguration->welcome_image = true;
            }else{
                $getConfiguration->welcome_image = false;
            }

            $getConfiguration->seew_video_link = $request->seew_video_link;
            $getConfiguration->peak_video_link = $request->peak_video_link;

            $getConfiguration->save();
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
