<?php

namespace App\Http\Controllers;

use App\Contactus;
use App\ContactusTranslation;
use App\Mail\ContactEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;

class ContactusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = '';
        if (Auth::check()){
            $contactuss = Contactus::all();
            return view('contactus.index', ['contactuss' => $contactuss]);
        }
        return redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contactus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'spanish_subject' => 'required',
            'english_subject' => 'required',
            'contactus_email' => 'required|email',
        ]);


        $newContactUs = new Contactus();

        $newContactUs->email = $request->contactus_email;
        $newContactUs->translateOrNew('es')->subject = $request->spanish_subject;
        $newContactUs->translateOrNew('en')->subject = $request->english_subject;

        $newContactUs->save();

        return redirect('contactus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contactus::find($id);
        return view('contactus.edit', ['contact' => $contact]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $contactEdit = Contactus::where('id', $id)->first();

        $this->validate($request, [
            'spanish_subject' => 'required',
            'english_subject' => 'required',
            'contactus_email' => 'required|email',
        ]);

        $contactEdit->translate('es')->subject = $request->spanish_subject;
        $contactEdit->translate('en')->subject = $request->english_subject;
        $contactEdit->email = $request->contactus_email;
        $contactEdit->save();

        return redirect('contactus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $contactus = Contactus::find($id);
        $contactus->delete();

        return response("Element deleted.", Response::HTTP_OK);
    }

    public function sendEmail(Request $request){
        $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
        if($locale == 'es'){
            $lang = 'es';
        }else{
            $lang = 'en';
        }

        $emailId = ContactusTranslation::where([['subject', $request->email_subject], ['locale', $lang]])->first();
        $emailTo = Contactus::find($emailId->contactus_id);
        $name = $request->user_name;
        $email = $request->user_email;
        $phone = $request->user_phone;
        $subject = $request->email_subject;
        $menssage = $request->user_message;

        Mail::to($emailTo->email)->send(new ContactEmail($name, $email, $phone, $subject, $menssage));
        return back();
    }
}


