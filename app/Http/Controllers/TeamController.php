<?php

namespace App\Http\Controllers;

use App\Member;
use App\Profession;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = '';
        if (Auth::check()){
            $members = Member::all();
            return view('team.index', ['members' => $members]);
        }
        return redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $professions = Profession::all();
        return view('team.create', ['roles' => $roles, 'professions' => $professions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fileName = str_random(5).time().'.'.$request->file('member_photo')->guessClientExtension();

        if(isset($request->member_name) && isset($request->member_last_name)){
            $request->request->add(['mmy_key' => 'member-'.$request->member_name.'-'.$request->member_last_name.'-name-last_name']);
        }

        $this->validate($request, [
            'mmy_key' => 'required|uniqueTwoElements',
            'member_web_address' => 'required',
            'member_profession' => 'required',
            'member_role' => 'required',
            'member_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $path = $request->file('member_photo')->storeAs(
            'team', $fileName, 'public'
        );

        Member::create([
            "name" => $request->member_name,
            "last_name" => $request->member_last_name,
            'web_address' => $request->member_web_address,
            'photo' => $fileName,
            'profession_id' => $request->member_profession,
            'role_id' => $request->member_role
        ]);

        return redirect('team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::find($id);
        $roles = Role::all();
        $professions = Profession::all();
        return view('team.edit', ['member' => $member, 'roles' => $roles, 'professions' => $professions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $memberToEdit = Member::find($id);

        if(isset($request->member_name) && isset($request->member_last_name)){
            $request->request->add(['mmmy_key' => 'member-'.$request->member_name.'-'.$request->member_last_name.'-'.$memberToEdit->id.'-name-last_name']);
        }

        $this->validate($request, [
            'mmmy_key' => 'required|uniqueTwoElementsEdit',
            'member_web_address' => 'required',
            'member_profession' => 'required',
            'member_role' => 'required',
            'member_photo' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        //Falta verificar q esto funcione bien
        if($request->file('member_photo')/*->getClientOriginalName()*/ !== NULL){
            if(file_exists(storage_path('app/public/team/'.$memberToEdit->photo))){
                unlink(storage_path('app/public/team/'.$memberToEdit->photo));
            }

            $fileName = str_random(5).time().'.'.$request->file('member_photo')->guessClientExtension();

            $path = $request->file('member_photo')->storeAs(
                'team', $fileName, 'public'
            );
        }else{
            $fileName = $memberToEdit->photo;
        }

        $memberToEdit->name = $request->member_name;
        $memberToEdit->last_name = $request->member_last_name;
        $memberToEdit->web_address = $request->member_web_address;
        $memberToEdit->photo = $fileName;
        $memberToEdit->profession_id = $request->member_profession;
        $memberToEdit->role_id = $request->member_role;

        $memberToEdit->save();
        return redirect('team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $member = Member::find($id);
        /*Storage::delete(storage_path('app/public/team/'.$member->photo)); It doesn't work*/
        if(file_exists(storage_path('app/public/team/'.$member->photo))){
            unlink(storage_path('app/public/team/'.$member->photo));
        }

        $member->delete();

        return response("Element deleted.", Response::HTTP_OK);
    }
}
