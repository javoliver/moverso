<?php

namespace App\Http\Controllers;

use App\Member;
use App\Personalities;
use App\Profession;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProfessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = '';
        if (Auth::check()){
            $professions = Profession::all();
            return view('profession.index', ['professions' => $professions]);
        }
        return redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profession.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'spanish_profession' => 'required|max:255|unique:profession,profession_es',
            'english_profession' => 'required|max:255|unique:profession,profession_en',
        ]);

        $professionToAdd = new Profession();

        $professionToAdd->profession_es = $request->spanish_profession;
        $professionToAdd->profession_en = $request->english_profession;

        $professionToAdd->save();

        return redirect('profession');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profession = Profession::find($id);
        return view('profession.edit', ['profession' => $profession]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $professionToEdit = Profession::find($id);

        $this->validate($request, [
            'spanish_profession' => 'required|max:255|unique:profession,profession_es,'.$professionToEdit->id,
            'english_profession' => 'required|max:255|unique:profession,profession_en,'.$professionToEdit->id,
        ]);

        $professionToEdit->profession_es = $request->spanish_profession;
        $professionToEdit->profession_en = $request->english_profession;

        $professionToEdit->save();
        return redirect('profession');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $personalities = DB::table('personalities')->where('profession_id', $id)->first();
        $member = DB::table('member')->where('profession_id', $id)->first();
        $profession = Profession::all();

        $profession = Profession::find($id);
        $response = new Response();
        if($personalities != NULL || $member != NULL){
            return response("Can not delete the profession because it is related with a member or a personality.", Response::HTTP_INTERNAL_SERVER_ERROR);
        }else{
            $profession->delete();
        }

        return response("Element deleted.", Response::HTTP_OK);
    }
}
