<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $error = '';
        if (Auth::check()){
            $videos = \App\Video::all();
            return view('videos.index', ['videos' => $videos]);
        }
        return redirect('login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('videos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $videos = Video::all();
        $equalTitle = false;
        $equalOrder = false;

        $this->validate($request,[
            'video_title' => 'required|unique:videos,title',
            'video_artists' => 'required',
            'making_off_url' => 'nullable|url',
            'video_url' => 'nullable|url',
            'video_category' => 'required',
            'video_priority' => 'sometimes|unique:videos,priority',
        ]);


        Video::create([
            'title' => $request->video_title,
            'artists' => $request->video_artists,
            'making_off_url' => $request->making_off_url,
            'video_url' => $request->video_url,
            'category' => $request->video_category,
            'priority' => $request->video_priority
        ]);

        return redirect('videos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::find($id);
        return view('videos.edit', ['video' => $video]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $videoToEdit = Video::find($id);

        $this->validate($request,[
            'video_title' => 'required|unique:videos,title,'.$videoToEdit->id,
            'video_artists' => 'required',
            'making_off_url' => 'nullable|url',
            'video_url' => 'nullable|url',
            'video_category' => 'required',
            'video_priority' => 'sometimes|unique:videos,priority,'.$videoToEdit->priority,
        ]);

        $videoToEdit->title = $request->video_title;
        $videoToEdit->artists = $request->video_artists;
        $videoToEdit->making_off_url = $request->making_off_url;
        $videoToEdit->video_url = $request->video_url;
        $videoToEdit->category = $request->video_category;
        $videoToEdit->priority = $request->video_priority;

        $videoToEdit->save();

        return redirect('videos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $video = Video::find($id);
        $video->delete();

        return response("Element deleted.", Response::HTTP_OK);
    }

}
