<?php

namespace App\Http\Controllers;

use App\Videophoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideophotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $this->validate($request, [
            'video_photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $fileName = str_random(5).time().'.'.$request->file('video_photo')->getClientOriginalExtension();

        $path = $request->file('video_photo')->storeAs(
            'video-photo', $fileName, 'public'
        );

        Videophoto::create([
            "photo" => $fileName,
            "category" => $request->photo_category,
            'video_id' => $request->video_id
        ]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        if($request->ajax()){
            $id = $request->id;
            $info = Videophoto::find($id);
            //echo json_decode($info);
            return response()->json($info);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->edit_id;
        $photoToEdit = Videophoto::find($id);
        $photoEntrance = new Videophoto();

        if($request->file('video_photo_edit')/*->getClientOriginalName()*/ !== NULL){

            $this->validate($request, [
                'video_photo_edit' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            if(file_exists(storage_path('app/public/video-photo/'.$photoToEdit->photo))){
                unlink(storage_path('app/public/video-photo/'.$photoToEdit->photo));
            }

            $fileName = str_random(5).time().'.'.$request->file('video_photo_edit')->getClientOriginalExtension();

            $path = $request->file('video_photo_edit')->storeAs(
                'video-photo', $fileName, 'public'
            );
            $photo = $fileName;
        }else{
            $photo = $photoToEdit->photo;
        }

        $photoToEdit->photo = $photo;
        $photoToEdit->category = $request->photo_category_edit;

        $photoToEdit->save();

        return redirect('videosphotoss/'.$request->video_id)->with('success','Record Updated successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Videophoto::find($id);
        $photo->delete();
        if(file_exists(storage_path('app/public/video-photo/'.$photo->photo))){
            unlink(storage_path('app/public/video-photo/'.$photo->photo));
        }

        return response("Element deleted.", 200);
    }

    public function photosofavideo(Request $request){
        if($request->ajax()){
            $id = $request->id;
            $photos = Videophoto::where('video_id', $id)->get();

            //echo json_decode($info);
            return response()->json($photos);
        }
    }
}
