<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personalities extends Model
{
    use \Dimsav\Translatable\Translatable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'personalities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'social_network_address', 'artist_work', 'work_interpreters', 'photo', 'photo_mobile', 'profession_id'
    ];

    /**
     * The attributes that are translatable
     */
    public $translatedAttributes = ['summary'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function profession()
    {
        return $this->belongsTo('App\Profession');
    }
}
