<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profession';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profession_es', 'profession_en'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    // each category can be asign to many personalities
    public function personalities()
    {
        return $this->hasMany('App\Personalities');
    }

    public function members()
    {
        return $this->hasMany('App\Member');
    }
}
