<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('uniqueTwoElements', function ($attribute, $value, $parameters, $validator) {
            $value_array = explode("-", $value);
            $items = DB::select("SELECT count(*) as aggregate FROM $value_array[0] WHERE $value_array[3] ='$value_array[1]' AND $value_array[4]='$value_array[2]'");
            $number=$items[0]->aggregate;
            if ($number > 0) {
                return false;
            } else {
                return true;
            }
        });

        Validator::extend('uniqueTwoElementsEdit', function ($attribute, $value, $parameters, $validator) {
            $value_array = explode("-", $value);
            $items = DB::select("SELECT count(*) as aggregate FROM $value_array[0] WHERE $value_array[4] ='$value_array[1]' AND $value_array[5]='$value_array[2]' AND id <> $value_array[3]");
            $number=$items[0]->aggregate;
            if ($number > 0) {
                return false;
            } else {
                return true;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
