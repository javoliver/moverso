<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videophoto extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'video_photo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'photo', 'category', 'video_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    //Defining relationship with Video
    public function videos() {
        return $this->belongsTo('App\Video');
    }
}
