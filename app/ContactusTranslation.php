<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactusTranslation extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['subject'];
}
