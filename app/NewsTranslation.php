<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTranslation extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];
    protected $fillable = ['title', 'description'];
}
