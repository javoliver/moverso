<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artists extends Model
{
    use \Dimsav\Translatable\Translatable;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'personality';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'social_network_address', 'category', 'summary','photo'
    ];

    /**
     * The attributes that are translatable
     */
    public $translatedAttributes = ['summary'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
