<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Whatwedo extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'what_we_do';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description_es', 'description_en'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];
}
